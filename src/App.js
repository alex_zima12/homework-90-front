import React, {useState, useRef, useEffect} from 'react';

const App = () => {

  const [state, setState] = useState({
    mouseDown: false,
    pixelsArray: []
  });

  const canvas = useRef(null);
  const ws = useRef(null);

  useEffect(() => {
    ws.current = new WebSocket("ws://localhost:8000/canvas");

    ws.current.onopen = () => {
      ws.current.send(JSON.stringify({type: "GET_ALL_PIXELS"}));
    };
    ws.current.onclose = () => console.log("ws connection closed");
    ws.current.onmessage = e => {
      const dataPixel = JSON.parse(e.data);
      if (dataPixel.type === "NEW_PIXEL") {
        setState(prevState => {
         return {
           ...prevState,
           pixelsArray: [...state.pixelsArray, dataPixel.pixels]
         }
        })
      } else if (dataPixel.type === "ALL_PIXELS") {
        setState(prevState => {
          return {
            ...prevState,
            pixelsArray: [...state.pixelsArray, ...dataPixel.pixels]
          }
        } );

        dataPixel.pixels.forEach(pixel => {
          const context = canvas.current.getContext('2d');
          context.beginPath();
          for (const [key, value] of Object.entries(pixel.pixels)) {
            if (value.x && value.y) {
              context.arc(value.x, value.y, true, 0, 2 * Math.PI);
              context.closePath();
              context.fill();
            }
          }
        });
      }
    };
    return () => ws.current.close();
  }, []);

  const canvasMouseMoveHandler = event => {

    if (state.mouseDown) {
      event.persist();
      const clientX = event.clientX;
      const clientY = event.clientY;
      setState(prevState => {
        return {
          ...prevState,
          pixelsArray: [...prevState.pixelsArray, {
            x: clientX,
            y: clientY
          }]
        };
      });

      const context = canvas.current.getContext('2d');
      const imageData = context.createImageData(1, 1);
      const d = imageData.data;

      d[0] = 0;
      d[1] = 0;
      d[2] = 0;
      d[3] = 255;
      context.putImageData(imageData, event.clientX, event.clientY);
    }
  };


  const mouseDownHandler = event => {
    setState({...state, mouseDown: true});
  };


  const mouseUpHandler = event => {

    ws.current.send(JSON.stringify({
        type: "CREATE_PICTURE",
        pixel: state.pixelsArray
      }));
    console.log("отправка на сервер",JSON.stringify({
      type: "CREATE_PICTURE",
      pixel: state.pixelsArray
    }) );
    setState({
      ...state,
      mouseDown: false,
      pixelsArray: []
    });

  };

  return (
      <div>
        <canvas
            ref={canvas}
            style={{border: '1px solid black'}}
            width={800}
            height={600}
            onMouseDown={mouseDownHandler}
            onMouseUp={mouseUpHandler}
            onMouseMove={canvasMouseMoveHandler}
        />

      </div>
  );

};

export default App;
